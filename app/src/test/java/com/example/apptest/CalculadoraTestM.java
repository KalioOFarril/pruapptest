package com.example.apptest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CalculadoraTestM {

    private Calculadora calc;

    @Before
    public void setUp() throws Exception {
        calc = mock(Calculadora.class);
        when(calc.suma(2,2)).thenReturn(4);
        when(calc.resta(2,4)).thenReturn(-2);
        when(calc.mulitplicacion(3,5)).thenReturn(15);
        when(calc.division(10,2)).thenReturn(5);
        when(calc.division(10,0)).thenThrow(ArithmeticException.class);
    }

    //@Ignore
    @Test(expected = ArithmeticException.class)
    public void divisionMEx2() throws Exception{
        int res = calc.division(10,0);
    }

    @Test
    public void sumaM() {
        calc.suma(2,2);
        verify(calc).suma(2,2);
    }

    @Test
    public void restaM() {
        calc.resta(2,4);
        verify(calc).resta(2,4);
    }

    @Test
    public void mulitplicacionM() {
        calc.mulitplicacion(3,5);
        verify(calc).mulitplicacion(3,5);
    }

    @Test
    public void divisionM() {
        calc.division(10,2);
        verify(calc).division(10,2);
    }

    @Test(expected = ArithmeticException.class)
    public void divisionMEx(){
        int res = calc.division(10,0);
    }

    @Test
    public void sumaMatLeast() throws Exception{
        calc.suma(2,2);
        calc.suma(2,4);
        verify(calc, atLeast(2)).suma(anyInt(), anyInt());
    }

    @Test
    public void sumaMnever() throws Exception{
        verify(calc, never()).suma(2,6);
    }
}