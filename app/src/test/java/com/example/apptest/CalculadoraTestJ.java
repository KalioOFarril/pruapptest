package com.example.apptest;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculadoraTestJ {

    private static Calculadora calc;

    @BeforeClass
    public static void setUp() throws Exception{
        calc = new Calculadora();
    }

    @Test
    public void sumaJ() throws Exception{
        int res = calc.suma(2,2);
        int resEsperado = 4;
        Assert.assertEquals(resEsperado,res);
    }

    @Test
    public void restaJ() throws Exception{
        int res = calc.resta(2,4);
        int resEsperado = -2;
        Assert.assertEquals(resEsperado,res);
    }

    @Test
    public void mulitplicacionJ() throws Exception{
        int res = calc.mulitplicacion(3,5);
        int resEsperado = 15;
        Assert.assertEquals(resEsperado,res);
    }

    @Test
    public void divisionJ() throws Exception{
        int res = calc.division(10,2);
        int resEsperado = 5;
        Assert.assertEquals(resEsperado,res);
    }

    @Test
    public void sumaJNotEqual() throws Exception{
        int res = calc.suma(2,2);
        int resEsperado = 5;
        Assert.assertNotEquals(resEsperado,res);
    }

    @Test
    public void sumaJTrue() throws Exception{
        int res = calc.suma(2,2);
        int resEsperado = 4;
        Assert.assertTrue(resEsperado == res);
    }

    @Ignore
    @Test
    public void testQueFallara() throws Exception{
        fail("Falla porque esta pendiente de implementar");
    }
}