package com.example.apptest;

public class Calculadora {
    public int suma(int a,int b){
        return a+b;
    }

    public int resta(int a,int b){
        return a-b;
    }

    public int mulitplicacion(int a,int b){
        return a*b;
    }

    public int division(int a,int b){
        int residuo = 0;
        try{
            residuo = a/b;
        }catch(ArithmeticException ex){
            System.out.println("Error " + ex.getMessage());
            throw new IllegalArgumentException("Dividendo es cero");
        }
        return residuo;
    }
}
